'use strict';

const { Adw, Gio, Gtk } = imports.gi;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

function init() {
}

function fillPreferencesWindow(window) {
	// Use the same GSettings schema as in `extension.js`
	const settings = ExtensionUtils.getSettings('org.gnome.shell.extensions.docker');

	const page = new Adw.PreferencesPage();

	// GROUP: UI
	const group_ui = new Adw.PreferencesGroup();
	group_ui.set_title("UI");
	page.add(group_ui);


	// Menu type
	const row_menu_type = new Adw.ActionRow({
		title: settings.settings_schema.get_key("menu-type").get_summary(),
		subtitle: settings.settings_schema.get_key("menu-type").get_description(),
	});
	group_ui.add(row_menu_type);

	const combobox_menu_type = new Gtk.ComboBoxText();
	combobox_menu_type.set_valign(Gtk.Align.CENTER);
	combobox_menu_type.append("text", "Text");
	combobox_menu_type.append("icons", "Icons");

	settings.bind(
		'menu-type',
		combobox_menu_type,
		'active-id',
		Gio.SettingsBindFlags.DEFAULT
	);

	row_menu_type.add_suffix(combobox_menu_type);
	row_menu_type.activatable_widget = combobox_menu_type;

	// Visible menus.
	const row_visible_menu = new Adw.ActionRow({
		title: settings.settings_schema.get_key("show-value").get_summary(),
		subtitle: settings.settings_schema.get_key("show-value").get_description(),
	});
	group_ui.add(row_visible_menu);

	const combobox_visible_menu = new Gtk.ComboBoxText();
	combobox_visible_menu.set_valign(Gtk.Align.CENTER);
	combobox_visible_menu.append("both", "Both");
	combobox_visible_menu.append("containers", "Containers");
	combobox_visible_menu.append("images", "Images");

	settings.bind(
		'show-value',
		combobox_visible_menu,
		'active-id',
		Gio.SettingsBindFlags.DEFAULT
	);

	row_visible_menu.add_suffix(combobox_visible_menu);
	row_visible_menu.activatable_widget = combobox_visible_menu;

	// Visible ports.
	const row_visible_ports = new Adw.ActionRow({
		title: settings.settings_schema.get_key("show-ports").get_summary(),
		subtitle: settings.settings_schema.get_key("show-ports").get_description(),
	});
	group_ui.add(row_visible_ports);

	const switch_visible_ports = new Gtk.Switch();
	switch_visible_ports.set_valign(Gtk.Align.CENTER);

	settings.bind(
		'show-ports',
		switch_visible_ports,
		'state',
		Gio.SettingsBindFlags.DEFAULT
	);

	row_visible_ports.add_suffix(switch_visible_ports);
	row_visible_ports.set_activatable_widget(switch_visible_ports);

	// GROUP: Technical
	const group_technical = new Adw.PreferencesGroup();
	group_technical.set_title("Technical");
	page.add(group_technical);

	// Terminal
	const row_terminal = new Adw.ActionRow({
		title: settings.settings_schema.get_key("terminal").get_summary(),
		subtitle: settings.settings_schema.get_key("terminal").get_description(),
	});
	group_technical.add(row_terminal)

	const input_terminal = new Gtk.Entry();
	input_terminal.set_valign(Gtk.Align.CENTER);

	settings.bind(
		'terminal',
		input_terminal,
		'text',
		Gio.SettingsBindFlags.DEFAULT
	);

	row_terminal.add_suffix(input_terminal);
	row_terminal.activatable_widget = input_terminal;

	// Add our page to the window
	window.add(page);
}
