# [Docker - Gnome Shell Extension](https://extensions.gnome.org/extension/5103/docker/)

The objective is to create and manage containers from a tray icon.

# Install

## Dependencies

 - gnome-shell-extension
 - gettext

```bash
git clone git@gitlab.com:stickman_0x00/gnome_shell_extension_docker.git
cd gnome_shell_extension_docker
make install
```

# Test

## Wayland

```bash
make run #dbus-run-session -- gnome-shell --nested --wayland
```

## Xorg

```bash
make reload
```

# TODO:

- [ ] Popup asking if user really wants to remove image/container.
- [ ] Popup to ask for arguments when starting a new image.
- [ ] Docker: define better labels for commands + transform to translatable text.
- [ ] Docker: use labels in every part of the code and not just sometimes.
- [X] Hide/Show the Ports list.
- [ ] Monochromatic Icon (Cause the Docker Extension is the only blue colored icon).
- [ ] Locate docker-compose directory (If container run using docker-compose)?
- [ ] Allow users to choose the size of the submenu.

# Resources

 - https://gjs.guide/extensions/
 - https://gjs-docs.gnome.org/
 - http://justperfection.channel.gitlab.io/how-to-create-a-gnome-extension-documentation/Document.html
 - https://wiki.gnome.org/Attic/GnomeShell/Extensions/Writing
 - [misc.extensionUtils](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/misc/extensionUtils.js)
 - [ui.modalDialog](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/modalDialog.js)
 - [ui.panelMenu](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/panelMenu.js)
 - [ui.popupMenu](https://gitlab.gnome.org/GNOME/gnome-shell/blob/main/js/ui/popupMenu.js)
 - https://gitlab.gnome.org/GNOME/gjs/-/blob/master/doc/Mapping.md
 - https://gitlab.gnome.org/GNOME/gjs/-/blob/master/doc/Modules.md
 - https://extensions.gnome.org/extension/1864/hakan-baysal-onur-agtas/
 - https://gitlab.gnome.org/jrahmatzadeh/just-perfection/-/tree/main/scripts
 - [Gtk Examples](https://python-gtk-3-tutorial.readthedocs.io/en/latest/)
 - [How to Create a GNOME Extension](https://medium.com/@justperfection.channel/how-to-create-a-gnome-extension-eb31b12e78d5)