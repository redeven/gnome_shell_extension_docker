'use strict';

const { GObject } = imports.gi;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const ContainerMenu = Me.imports.modules.containerMenu;

const Docker = Me.imports.lib.docker;

const Gettext = imports.gettext;
const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;

var Container_Menu = GObject.registerClass(
	class Container_Menu extends ContainerMenu.Container_Menu {
		_init(container) {
			super._init(container);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: 100px;`;

			switch (container.state) {
				case "running":
					this._shell = new PopupMenu.PopupMenuItem(_("Exec Bash"));
					this._shell.connect('activate', () => Docker.run_command(Docker.docker_commands.c_exec, container));
					this.menu.addMenuItem(this._shell);

					this._attach = new PopupMenu.PopupMenuItem(_("Attach Terminal"));
					this._attach.connect('activate', () => Docker.run_command(Docker.docker_commands.c_attach, container));
					this.menu.addMenuItem(this._attach);

					this._pause = new PopupMenu.PopupMenuItem(_("Pause"));
					this._pause.connect('activate', () => Docker.run_command(Docker.docker_commands.c_pause, container));
					this.menu.addMenuItem(this._pause);

					this._stop = new PopupMenu.PopupMenuItem(_("Stop"));
					this._stop.connect('activate', () => Docker.run_command(Docker.docker_commands.c_stop, container));
					this.menu.addMenuItem(this._stop);

					this._restart = new PopupMenu.PopupMenuItem(_("Restart"));
					this._restart.connect('activate', () => Docker.run_command(Docker.docker_commands.c_restart, container));
					this.menu.addMenuItem(this._restart);
					break;

				case "paused":
					this._unpause = new PopupMenu.PopupMenuItem(_("Unpause"));
					this._unpause.connect('activate', () => Docker.run_command(Docker.docker_commands.c_unpause, container));
					this.menu.addMenuItem(this._unpause);

					this._stop = new PopupMenu.PopupMenuItem(_("Stop"));
					this._stop.connect('activate', () => Docker.run_command(Docker.docker_commands.c_stop, container));
					this.menu.addMenuItem(this._stop);

					this._restart = new PopupMenu.PopupMenuItem(_("Restart"));
					this._restart.connect('activate', () => Docker.run_command(Docker.docker_commands.c_restart, container));
					this.menu.addMenuItem(this._restart);
					break;

				default:
					this._start = new PopupMenu.PopupMenuItem(_("Start"));
					this._start.connect('activate', () => Docker.run_command(Docker.docker_commands.c_start, container));
					this.menu.addMenuItem(this._start);

					this._start_i = new PopupMenu.PopupMenuItem(_("Start interactive"));
					this._start_i.connect('activate', () => Docker.run_command(Docker.docker_commands.c_start_i, container));
					this.menu.addMenuItem(this._start_i);
			}

			this._logs = new PopupMenu.PopupMenuItem(_("View Logs"));
			this._logs.connect('activate', () => Docker.run_command(Docker.docker_commands.c_logs, container));
			this.menu.addMenuItem(this._logs);

			this._inspect = new PopupMenu.PopupMenuItem(_("Inspect"));
			this._inspect.connect('activate', () => Docker.run_command(Docker.docker_commands.c_inspect, container));
			this.menu.addMenuItem(this._inspect);

			this._remove = new PopupMenu.PopupMenuItem(_("Remove"));
			this._remove.connect('activate', () => Docker.run_command(Docker.docker_commands.c_rm, container));
			this.menu.addMenuItem(this._remove);

			this.add_ports();
		}
	}
)