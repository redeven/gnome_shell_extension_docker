'use strict';

const { GObject } = imports.gi;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Docker = Me.imports.lib.docker;

const Gettext = imports.gettext;
const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;

var Image_Menu = GObject.registerClass(
	class Image_Menu extends PopupMenu.PopupSubMenuMenuItem {
		_init(image) {
			super._init(image.name);

			// Set size of sub menu. !important
			this.menu.actor.style = `min-height: 100px;`;

			this._remove = new PopupMenu.PopupMenuItem(_("Run"));
			this._remove.connect('activate', () => Docker.run_command(Docker.docker_commands.i_run, image));
			this.menu.addMenuItem(this._remove);

			this._remove = new PopupMenu.PopupMenuItem(_("Run interactive"));
			this._remove.connect('activate', () => Docker.run_command(Docker.docker_commands.i_run_i, image));
			this.menu.addMenuItem(this._remove);

			this._inspect = new PopupMenu.PopupMenuItem(_("Inspect"));
			this._inspect.connect('activate', () => Docker.run_command(Docker.docker_commands.i_inspect, image));
			this.menu.addMenuItem(this._inspect);

			this._remove = new PopupMenu.PopupMenuItem(_("Remove (force)"));
			this._remove.connect('activate', () => Docker.run_command(Docker.docker_commands.i_rm, image));
			this.menu.addMenuItem(this._remove);
		}
	}
)