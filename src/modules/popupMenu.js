'use strict';

const { Clutter, GObject, St } = imports.gi;
const PopupMenu = imports.ui.popupMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Tooltip = Me.imports.lib.tooltip;

var PopupSubMenuMenuItem = GObject.registerClass(
	class PopupSubMenuMenuItem extends PopupMenu.PopupSubMenuMenuItem {
		_init(name) {
			super._init(name);
			this.buttons = 0;
		}

		new_action_button(icon, onClickAction, tooltip) {
			// Calculate button row, 3 buttons per row
			let menu = "_menu" + ~~(this.buttons / 3);
			if (this.buttons % 3 === 0) {
				this[menu] = new PopupMenu.PopupBaseMenuItem({ reactive: false, can_focus: false });
				this.menu.addMenuItem(this[menu]);
			}

			this[tooltip] = new St.Button({
				track_hover: true,
				style_class: "button",
				x_expand: true,
				x_align: Clutter.ActorAlign.CENTER
			});

			this[tooltip].child = new St.Icon({
				icon_name: icon,
				style_class: "popup-menu-icon"
			});

			this[tooltip].tooltip = new Tooltip.Tooltip({
				parent: this[tooltip],
				markup: tooltip,
				y_offset: 35
			});

			this[tooltip].connect('clicked', () => {
				onClickAction();
				this._parent._parent.close();
			});

			this[menu].actor.add_child(this[tooltip]);
			this.buttons++;
		}
	}
)

var PopupMenuScrollSection = class PopupMenuScrollSection extends PopupMenu.PopupMenuSection {
	constructor() {
		super();
		this.actor = new St.ScrollView({
			y_align: St.Align.START,
			overlay_scrollbars: false,
		});
		this.actor.add_actor(this.box);
		this.actor._delegate = this;
	}
};