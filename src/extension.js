'use strict';

const Main = imports.ui.main;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();


class Extension {
	constructor(uuid) {
		this._uuid = uuid;

		// Initialize translations
		ExtensionUtils.initTranslations(Me.metadata.uuid);
	}

	enable() {
		// Create indicator.
		this._indicator = new Me.imports.modules.menu.Menu;

		Main.panel.addToStatusArea(this._uuid, this._indicator);
	}

	disable() {
		this._indicator.destroy();
		this._indicator = null;
	}
}


function init(meta) {
	return new Extension(meta.uuid);
}
